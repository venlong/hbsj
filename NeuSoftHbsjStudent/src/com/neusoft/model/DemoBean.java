package com.neusoft.model;

import java.io.Serializable;

public class DemoBean implements Serializable {

	private static final long serialVersionUID = 1L;
	private String SaleManUserID;
	private String CompanyName;
	private String CompanyAddress;
	private String Phone;
	private String Fee;
	private String Money;
	private String EndDate;
	private String GetTicketMoney;
	private String GetTicketMemo;
	private String UserID;
	
	
	public String getUserID() {
		return UserID;
	}

	public void setUserID(String userID) {
		UserID = userID;
	}

	public String getSaleManUserID() {
		return SaleManUserID;
	}

	public void setSaleManUserID(String saleManUserID) {
		SaleManUserID = saleManUserID;
	}

	public String getCompanyName() {
		return CompanyName;
	}

	public void setCompanyName(String companyName) {
		CompanyName = companyName;
	}

	public String getCompanyAddress() {
		return CompanyAddress;
	}

	public void setCompanyAddress(String companyAddress) {
		CompanyAddress = companyAddress;
	}

	public String getPhone() {
		return Phone;
	}

	public void setPhone(String phone) {
		Phone = phone;
	}

	public String getFee() {
		return Fee;
	}

	public void setFee(String fee) {
		Fee = fee;
	}

	public String getMoney() {
		return Money;
	}

	public void setMoney(String money) {
		Money = money;
	}

	public String getEndDate() {
		return EndDate;
	}

	public void setEndDate(String endDate) {
		EndDate = endDate;
	}

	public String getGetTicketMoney() {
		return GetTicketMoney;
	}

	public void setGetTicketMoney(String getTicketMoney) {
		GetTicketMoney = getTicketMoney;
	}

	public String getGetTicketMemo() {
		return GetTicketMemo;
	}

	public void setGetTicketMemo(String getTicketMemo) {
		GetTicketMemo = getTicketMemo;
	}

}
